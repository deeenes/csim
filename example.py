import csim

# instantiate the main class:
cs = csim.ClusteringSet()

# read clusterings data from multiple files:
cs.add_file_list(['clustering1.csv', 'clustering2.csv'],
                 param = {'header': True, 'sep': ';', 'cols': [1]})

# obtain a similarity matrix using the igraph implementation of
# Danon adjusted mutual information:
sim = cs.danon_mi_igraph()
