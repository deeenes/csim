cimport cython
from libc.stdlib cimport malloc, free

from cpython cimport array
import array

import numpy as np
cimport numpy as np

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
def sub2li(int n, int i):
    return int(n * i - 3 * i / 2.0 - i ** 2 / 2.0 - 1)

#def comem(mi):
    
    #cdef int *mii
    #mii = <int *>malloc(len(a)*cython.sizeof(np.int))
    #if mii is NULL:
        #raise MemoryError()
    #for i in xrange(len(mi)):
        #mii[i] = mi[i]
    
    #with nogil:
        #for i, ii in enumerate(mi[:-1]):
            #int ri = sub2li(n, i)
            ## ri = n * i - 3 * i / 2 - i ** 2 / 2 - 1
            #for j in mi[i + 1:]:
                #int li = int(ri + j)
                #cmem[li] = 1

def lst(l):
    
    cdef array.array lc = array.array('i', l)
    
    lc[2] = 66
    
    return list(lc)

ctypedef np.int32_t DTYPE_t

@cython.boundscheck(False)
def aaa(
    np.ndarray[DTYPE_t, ndim = 1] a1,
    # np.ndarray a2
    ):
    cdef int a1s = a1.shape[0]
    # cdef int a2s = a2.shape[0]
    cdef long[:] na = np.zeros((a1s,), dtype = np.long)
    na[2] = <unsigned int>(a1[2])
    for i in xrange(a1s):
        # na[i] = a1[i]
        pass
    
    return np.array(na)

@cython.boundscheck(False)
def comem(
    np.ndarray[DTYPE_t, ndim = 1] a1,
    np.ndarray[DTYPE_t, ndim = 1] a2,
    ):
    
    cdef unsigned int a1s = a1.shape[0]
    cdef unsigned int a2s = a2.shape[0]
    
    cdef unsigned int n11, n10, n01, n00
    n11 = n10 = n01 = n00 = 0
    cdef unsigned int i, j, j0
    
    for i in range(0, a1s - 1):
        j0 = i + 1
        for j in range(j0, a2s):
            if a1[i] == a1[j] and a2[i] == a2[j]:
                n11 += 1
            elif a1[i] == a1[j]:
                n10 += 1
            elif a2[i] == a2[j]:
                n01 += 1
            else:
                n00 += 1
    
    return n11, n10, n01, n00

@cython.boundscheck(False)
def comembership(c):
    """
    Returns comembership vector, where each value tells if one pair
    of entites belong to the same group (1) or not (0).
    """
    cdef int n = len(c.memberships)
    cdef int cnum = c.cnum
    cdef int ri, i, ii, j, li
    
    cdef unsigned char[:] cmem = np.zeros((int(n * (n - 1) / 2), ), dtype = np.uint8)
    
    for ci in xrange(cnum):
        mi = c.members[ci]
        for i in xrange(len(mi) - 1):
            ii = mi[i]
            ri = n * ii - 3 * ii / 2 - ii ** 2 / 2 - 1
            for j in mi[i+1:]:
                li = ri + j
                cmem[li] = 1
    
    return np.array(cmem)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
def pair_counts(
    np.ndarray[DTYPE_t, ndim = 1] a,
    np.ndarray[DTYPE_t, ndim = 1] b
    ):
    
    cdef unsigned int n, n11, n10, n01, n00
    
    n = a.shape[0]  # also b.shape[0]
    
    cdef np.ndarray[long, ndim = 1] counts_a = np.bincount(a)
    cdef np.ndarray[long, ndim = 1] counts_b = np.bincount(b)
    cdef np.ndarray[long, ndim = 1] sorter_a = np.argsort(a)
    
    n11 = 0
    cdef np.ndarray[long, ndim = 1] same_a_offset = np.cumsum(counts_a)
    for indices in np.split(sorter_a, same_a_offset):
        b_check = b[indices]
        n11 += np.count_nonzero(b_check == b_check[:,None])
    
    n11 = (n11-n) // 2
    n10 = (np.sum(counts_a**2) - n) // 2 - n11
    n01 = (np.sum(counts_b**2) - n) // 2 - n11
    n00 = (n**2 - n) // 2 - n11 - n10 - n01
    
    return n11, n10, n01, n00

"""
import csim
from csim import comem
import numpy as np
a1 = np.array([1,1,1,2,2,3,3,4,4])
a2 = np.array([1,1,2,2,2,3,5,5,5])
comem.comem(a1, a2)
"""

    #cdef int *lc
    #lc = <int *>malloc(len(l) * cython.sizeof(int))
    #if lc is NULL:
        #raise MemoryError()
    #for i in xrange(len(l)):
        #lc[i] = l[i]
    #with nogil:
        
        #lc[2] = 66
    
    #return <int[:cython.sizeof(int)]> (compute(*lc))

    ##re
    
    ##for i in xrange(cython.sizeof(lc) / cython.sizeof(int)):
        ##ln[i] = lc[i]
    
    ##free(lc)
    
    ##return ln

"""
cython --embed comem.pyx
#/usr/bin/gcc -I/usr/include/python3.5m/ -c comem.c -o comem.o
/usr/bin/gcc -shared -fPIC -Os -I/usr/include/python3.5m/ -I/usr/lib/python3.5/site-packages/numpy/core/include/ -o comem.so comem.c
#/usr/bin/gcc -lpython3.5m -march=i386 -g comem.o -o comem.so
"""
