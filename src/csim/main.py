#!/usr/bin/python3
# -*- coding: utf-8 -*-

#
#  This file is part of the `csim` Python module
#
#  Copyright (c) 2016 - EMBL-EBI
#
#  File author(s): Dénes Türei (turei.denes@gmail.com)
#
#  Distributed under the GPLv3 License.
#  See accompanying file LICENSE.txt or copy at
#      http://www.gnu.org/licenses/gpl-3.0.html
#
#  Website: http://www.ebi.ac.uk/~denes
#

"""
This module is based on the following guides:
    -- Silke Wagner and Dorothea Wagner: Comparing Clusterings - An Overview
"""

from future.utils import iteritems
from past.builtins import xrange, range, reduce
from collections import Counter

import os
import sys
import imp
import numpy as np
import scipy as sp
import scipy.misc
import sklearn.utils
import itertools
import warnings
import wrapt
import gzip

try:
    import rpy2.robjects.packages as rpackages
    rbase = rpackages.importr('base')
except:
    sys.stdout.write('No `rpy2` available, R based methods'\
                     ' won\'t be accessible.\n')

try:
    clue = rpackages.importr('clue')
except:
    sys.stdout.write('No R package `clue` available, '\
                     'clue methods won\'t be accessible.\n')

import csim.progress as progress

try:
    import csim.comem as comem
except:
    pass

try:
    import igraph
except:
    sys.stdout.write('Note: no module `igraph` available.\n'\
        'Method implementations from\n'\
        '\t`igraph.clustering.compare_communities()`\n'\
        'won\'t be available.\n')

try:
    import seaborn as sns
except:
    sys.stdout.write('Note: no module `seaborn` available.\n'\
        'Plotting heatmap won\'t work without this.\n')

class Reader(object):
    
    """
    Reads clustering data from file.
    """
    
    def __init__(self, fname, cols = 0, sep = '\t', names = None,
        header = False, labels = None, members_list = False, clnames = None):
        """
        The reading is performed on initialization, the data
        will be accessible in the attribute `clusterings`.
        
        :param str fname: The filename or path.
        :param cols: Column number(s) to read. Default is the 1st column.
        `None` results the reading of all columns.
        Each column will be read as a clustering.
        :param str sep: Field separator. Default is tab.
        :param names: Names for each of the clusterings as a list
        or the only one as a string. Optional.
        :param bool header: Whether if the first row of the file is a header.
        :param list labels: List of labels for individual entities. Optional.
        :param bool members_list: Whether the file format is a table of mem-
        bership vectors as columns (False) or each line represents a cluster
        and lists its members (True).
        :param str clnames: Column index of cluster names if `members_list`
        is True.
        """
        for attr, val in iteritems(locals()):
            setattr(self, attr, val)
        self.clusterings = []
        self.read()
    
    def reload(self):
        modname = self.__class__.__module__
        mod = __import__(modname, fromlist = [modname.split('.')[0]])
        imp.reload(mod)
        new = getattr(mod, self.__class__.__name__)
        setattr(self, '__class__', new)
    
    def read(self):
        """
        Performs the reading. Called automatically on instantiation.
        """
        self.clusterings = []
        
        if not os.path.exists(self.fname):
            sys.stdout.write('No such file: `%s`.\n' % self.fname)
            return None
        
        if self.fname[-2:] == 'gz':
            fobj = gzip.open(self.fname, 'r')
        else:
            fobj = open(self.fname, 'r')
        
        
        self.data = list(map(lambda l: l.strip().split(self.sep),
                         map(lambda l: l.decode('utf-8')
                                       if type(l) is bytes else l,
                         fobj)))
        
        if self.header:
            self.hline = self.data[0]
            self.data = self.data[1:]
        
        fobj.close()
        
        if self.cols is None:
            self.cols = list(xrange(len(self.data[0])))
        
        if type(self.cols) is int:
            self.cols = [self.cols]
        
        if self.names is None:
            self.names = self.fname.split(os.sep)[-1]
        
        if type(self.names) is not list:
            if len(self.cols) > 1:
                self.names = \
                    list(
                        map(
                            lambda c:
                                '%s#%u' % (self.names, c),
                            self.cols
                        )
                    )
            else:
                self.names = [self.names]
        
        if self.members_list:
            
            clnames = list(map(lambda l: l[self.clnames], self.data)) \
                if self.clnames is not None else list(range(len(self.data)))
            
            memberships, self.labels = \
                zip(
                    *list(
                        map(
                            lambda c:
                                (
                                    [c[0]] * len(c[1][self.cols[0]:]),
                                    c[1][self.cols[0]:]
                                ),
                            zip(clnames, self.data)
                        )
                    )
                )
            
            memberships = list(itertools.chain.from_iterable(memberships))
            self.labels = list(itertools.chain.from_iterable(self.labels))
            
            memberships = \
                list(
                    map(
                        lambda i:
                            i[0],
                        sorted(
                            zip(memberships, self.labels),
                            key = lambda i: i[1]
                        )
                    )
                )
            self.labels = sorted(self.labels)
            
            self.clusterings = [Clustering(memberships,
                                           labels = self.labels,
                                           name = self.names[0])]
            
        else:
            
            self.clusterings.extend(
                list(
                    map(
                        lambda c:
                            Clustering(
                                list(
                                    map(
                                        lambda l:
                                            l[c[0]],
                                        self.data
                                    )
                                ),
                                labels = self.labels,
                                name = c[1]
                            ),
                        zip(self.cols, self.names)
                    )
                )
            )

#
#
#

class ClusteringSet(object):
    
    """
    Represents a set of different clusterings over the same set of entities.
    """
    
    def __init__(self, inputs = []):
        """
        Method `add_clusterings()` called on initialization, ot you can add
        clusterings later using that method.
        """
        self.clusterings = []
        self.lengths_unequal = False
        self._create_methods()
        self.add_clusterings(inputs)
        self.results = {}
        self.plots = {}
    
    def reload(self):
        modname = self.__class__.__module__
        mod = __import__(modname, fromlist = [modname.split('.')[0]])
        imp.reload(mod)
        new = getattr(mod, self.__class__.__name__)
        setattr(self, '__class__', new)
    
    def add_clusterings(self, inputs):
        """
        Adds clusterings to this set.
        
        :param list inputs: Either a list of `Clustering` objects, or
        a list of dicts with parameters for `Reader`. For `Reader` only
        filename is mandatory, so a minimal example of this case:
            cs.add_clusterings([{'fname': 'c1.txt'}, {'fname': 'c2.txt'}])
        
        """
        for inp in inputs:
            if inp.__class__ is Clustering:
                self.clusterings.append(inp)
            else:
                reader = Reader(**inp)
                self.clusterings.extend(reader.clusterings)
        if not self.test_lengths():
            warnings.warn('Not all clusterings are the same lenght. '\
                'Some methods will return `nan`.')
    
    def test_lengths(self):
        """
        Tests if all clusterings are of equal lengths.
        """
        lens = set(map(lambda cl: len(cl), self.clusterings))
        return len(lens) != 1
    
    def add_file_list(self, files, param = {}):
        """
        Reads clusterings from a list of files with the same input parameters.
        
        :param list files: List of filenames (paths).
        :param dict param: Arguments for Reader as a dict.
        """
        for fname in files:
            reader = Reader(fname, **param)
            self.clusterings.extend(reader.clusterings)
    
    def build_matrix(self, method, **kwargs):
        """
        This is a generic method for calculation of one distance or
        similarity measure between all pairs of clusterings in this set.
        """
        numofcl = len(self.clusterings)
        result = np.empty((numofcl, numofcl))
        result[:] = np.nan
        
        if numofcl == 0:
            return result
        
        if not hasattr(self.clusterings[0], method):
            sys.stdout.write('No such method: %s\n' % method)
            return None
        
        prg = progress.Progress(
            numofcl * numofcl / 2.0,
            'Calculating similarities',
            1,
            percent = False
        )
        
        for i in xrange(numofcl):
            for j in xrange(i, numofcl):
                prg.step()
                ci = self.clusterings[i]
                cj = self.clusterings[j]
                result[i,j] = getattr(ci, method)(other = cj, **kwargs)
                if i != j:
                    result[j,i] = result[i,j] \
                        if method in ci.symmetric \
                        else getattr(cj, method)(other = ci)
        
        prg.terminate()
        
        return result
    
    def _create_method(self, method):
        def _method(self, return_result = False, **kwargs):
            self.results[method] = self.build_matrix(method, **kwargs)
            if return_result:
                return self.results[method]
        
        doc = """Creates matrix of the metric below calculated
        between each pairs of clusterings in this set.
        The matrix will be saved to `results[<method name>]`,
        and returned if `return_result` is True.\n\n"""
        
        _method.__doc__ = '%s%s' % (doc, getattr(Clustering, method).__doc__)
        _method.__name__ = method
        
        setattr(self, method, _method.__get__(self, self.__class__))
    
    def _create_methods(self):
        for method in Clustering.symmetric + Clustering.non_symmetric:
            self._create_method(method)
    
    def heatmap(self, meth, fname = None, title = None,
                context = {}, name_format = lambda x: x,
                nan_sub = 0.0,
                **kwargs):
        """
        Plots heatmap from similarity/distance matrix.
        The plot will be stored in `plots[<method name>]`.
        
        :param str method: The method to calculate similarities. If the method
        has not been run yet it will be called automatically.
        :param str fname: Filename. If not None the plot will be saved here.
        :param str title: Main title for the plot.
        If None, will be automatically generated.
        :param callable name_format: Method to format the names. 
                                     This to be called on clustering names.
        :param float nan_sub: Replace the NaN values in the matrix with this.
        :param dict context: To be passed to `seaborn.set_context()`.
        :param **kwargs: Keyword arguments passed to `seaborn.heatmap()`.
        """
        if meth not in self.results:
            getattr(self, meth)()
        
        data = self.results[meth]
        
        nans = np.where(np.isnan(data))
        if len(nans):
            sys.stdout.write('\t:: Warning: NaN values in distance matrix, '\
                'substituting with %.02f.\n' % nan_sub)
            sys.stdout.flush()
            data[nans] = nan_sub
        
        title = ('Similarity of clusterings: %s' % meth) \
            if title is None else title
        
        cnames = list(map(lambda c: name_format(c.name), self.clusterings))
        
        defaults = {
            'cmap': 'YlGnBu',
            'linewidths': .5,
            'vmin': 0.0,
            'vmax': 1.0,
            'yticklabels': cnames,
            'xticklabels': cnames
        }
        
        for k, v in iteritems(defaults):
            if k not in kwargs:
                kwargs[k] = v
        
        if 'sns' not in globals():
            raise Warning('No module `seaborn` is available,'\
                          'can not plot heatmap.')
        else:
            sns.set_context(context)
            grid = sns.clustermap(data, **kwargs)
            sns.plt.setp(grid.ax_heatmap.xaxis.get_majorticklabels(), rotation = 90)
            sns.plt.setp(grid.ax_heatmap.yaxis.get_majorticklabels(), rotation = 0)
            grid.fig.suptitle(title)
            self.plots[meth] = grid
            if fname is not None:
                # sns.plt.tight_layout()
                #fig = self.plots[method].get_figure()
                #fig.savefig(fname)
                self.plots[meth].savefig(fname)
                sns.plt.close()
                sns.plt.close('all')
    
    def save_matrix(self, method, fname, sep = "\t", labels = True,
                    name_format = lambda x: x):
        """
        Writes the similarity/distance matrix to text file.
        
        :param str method: The method to calculate similarities. If the method
        has not been run yet it will be called automatically.
        :param str fname: Filename for the output file.
        :param str sep: Field separator.
        :param bool labels: Whether to add row and column labels.
        :param callable name_format: Method to format the names.
                                     This to be called on clustering names.
        """
        if method not in self.results:
            getattr(self, method)()
        
        data = self.results[method]
        
        cnames = list(map(lambda c: name_format(c.name), self.clusterings))
        
        with open(fname, 'w') as f:
            f.write('%s%s\n' % (sep, sep.join(cnames)))
            f.write(
                '\n'.join(
                    map(
                        lambda l:
                            '%s%s%s' % (
                                l[0],
                                sep,
                                sep.join(
                                    map(
                                        lambda n:
                                            '%.06f' % n,
                                        l[1]
                                    )
                                )
                            ),
                        zip(cnames, data)
                    )
                )
            )
    
    def methods_scatterplot(self, m1, m2, fname = None, title = None,
                            xlab = None, ylab = None,
                            context = None,
                            rc = None,
                            font_scale = 1,
                            **kwargs):
        """
        Plots a scatterplot to compare 2 methods.
        """
        if m1 not in self.results: getattr(self, m1)()
        if m2 not in self.results: getattr(self, m2)()
        
        x = self.results[m1].flatten()
        y = self.results[m2].flatten()
        
        sns.set_context(context, font_scale, rc)
        ax = sns.regplot(x, y, **kwargs)
        if xlab is not None: ax.set(xlabel = xlab)
        if ylab is not None: ax.set(ylabel = ylab)
        if title is not None: ax.set_title(title)
        
        if fname is not None:
            sns.plt.tight_layout()
            fig = ax.get_figure()
            fig.savefig(fname)
            sns.plt.close()
            sns.plt.close('all')
        
        self.plots['%s_%s' % (m1, m2)] = ax
    
    def all_names(self):
        """
        Returns the names of all entities from all clusterings.
        """
        return \
            list(
                reduce(
                    lambda s1, s2:
                        s1 | s2,
                    map(
                        lambda c:
                        set(c.labels),
                        self.clusterings),
                set([])
                )
            )
    
    def maxlen(self):
        """
        Returns the length of the clustering
        which covers the highest number of entities.
        """
        return max(map(len, self.clusterings))
    
    def full_matrix(self):
        """
        Creates a matrix with clusterings as columns, the entities as rows;
        values are the cluster ID if the entity is in the clustering,
        else `numpy.nan` values.
        The array, rownames and colnames are assigned to `full`,
        `names` and `cnames` attributes respectively.
        """
        self.names = sorted(self.all_names())
        self.cnames = list(map(lambda c: c.name, self.clusterings))
        self.full = np.zeros([len(self.names),
                              len(self.clusterings)],
                              dtype = np.float)
        
        for i, name in enumerate(self.names):
            for j, c in enumerate(self.clusterings):
                if name in c.bylabel:
                    self.full[i, j] = c.bylabel[name]
                else:
                    self.full[i, j] = np.nan
    
    def to_clue_cl_ensemble(self):
        """
        Returns a clustering ensemble object. This is an R object defined in
        the CLUE R package.
        """
        rparts = []
        
        for i, name in enumerate(self.cnames):
            pa = clue.as_cl_partition(
                 clue.as_cl_membership(
                     rbase.c(*list(self.full[:,i]))
                 ))
            pa.rx2('.Meta').rx2['name'] = name
            rparts.append(pa)
        
        self.clue_cl_ensemble = clue.as_cl_ensemble(*rparts)
    
    def clue_cl_consensus(self, method = None, param = {}):
        """
        Obtains a consensus clustering by calling CLUE `cl_consensus` method.
        The result assigned to `consensus` attribute, the order of the
        membership vector is the same as the names in `names` attribute,
        while the cl_consensus R object will be in attribute `cl_consensus`.
        """
        if not hasattr(self, 'clue_cl_ensemble') or \
            self.clue_cl_ensemble is None:
            
            self.to_clue_cl_ensemble()
        
        method = method if method is not None else rpy2.rinterface.NULL
        
        control = rbase.list()
        
        for k, v in iteritems(param):
            control.rx2[k] = v
        
        self.cl_consensus = clue.cl_consensus(self.clue_cl_ensemble,
                                              method = method,
                                              control = control)
        
        self.consensus = \
            list(
                rbase.apply(
                    self.cl_consensus.rx2('.Data'), 1,
                    rbase.which_max
                )
            )
    
@wrapt.decorator
def check_lengths(func, instance, args, kwargs):
    other = args[0] if len(args) else kwargs['other']
    if len(instance) == len(other):
        if func.__name__ not in instance.unequal_length and (
            np.nan in instance.memberships or
            np.nan in other.memberships
        ):
            return np.nan
        else:
            return func(*args, **kwargs)
    else:
        c1, c2 = instance.adjust_lengths(other)
        return getattr(c1, func.__name__)(c2, *args[1:], **kwargs)

class Clustering(object):
    
    """
    Class represents one clustering, optionally with name
    and labels for the elements.
    Implements many distance and similarity metrics for comparison with
    other `Clustering` objects.
    """
    
    symmetric = [
        'chi_squared',
        'rand',
        'rand_igraph',
        'rand_adjusted',
        'rand_adjusted_igraph',
        'fowlkes_mallows',
        'mirkin',
        'jaccard',
        'partition_difference',
        'maximum_match',
        'van_dongen',
        'mutual_information',
        'strehl_ghosh_mi',
        'fred_jain_mi',
        'danon_mi_igraph',
        'split_join_distance_igraph',
        'variation_of_information',
        'variation_of_information_igraph',
        'pearson'
    ]
    
    non_symmetric = ['f_measure', 'meila_heckerman']
    
    unequal_length = [
        'fred_jain_mi',
        'split_join_distance_igraph',
        'danon_mi_igraph',
        'variation_of_information_igraph',
        'igraph_method',
        'pearson',
        'pair_counts2'
    ]
    
    igraph_methods = [
        'split_join_distance_igraph',
        'danon_mi_igraph',
        'variation_of_information_igraph',
        'igraph_method'
    ]
    
    def __init__(self, memberships, labels = None, name = None):
        if 'igraph' in globals() and \
            type(memberships) is igraph.clustering.Clustering:
            memberships = memberships.membership
        self.name = name
        self.clusters = sorted(list(set(
            filter(lambda m: m is not None, memberships)
        )))
        self.cnum = len(self.clusters)
        self.clab2cnum = dict(zip(self.clusters, xrange(self.cnum)))
        self.memberships = \
            np.array(
                list(
                    map(
                        lambda m:
                            self.clab2cnum[m] if m in self.clab2cnum else np.nan,
                        memberships
                    )
                ),
                dtype = np.int32
            )
        self.sizes = Counter(filter(lambda m: not np.isnan(m), self.memberships))
        self.labels = labels
        if self.labels is not None and \
            len(self.memberships) == len(labels):
            self.bylabel = dict(zip(labels, xrange(len(self.memberships))))
        
        self.members = dict(map(lambda i: (i, []), xrange(self.cnum)))
        list(map(lambda m: self.members[m[1]].append(m[0]),
                 enumerate(self.memberships)))
        self.members = dict(map(lambda m: (m[0], np.array(sorted(m[1]), dtype = np.int)), iteritems(self.members)))
    
    def __len__(self):
        return len(self.memberships)
    
    def __getitem__(self, item):
        if type(item) is int and item < len(self):
            return self.memberships[item]
        elif self.labels is not None and item in self.bylabel:
            return self.memberships[self.bylabel[item]]
        else:
            raise KeyError(item)
    
    def reload(self):
        modname = self.__class__.__module__
        mod = __import__(modname, fromlist = [modname.split('.')[0]])
        imp.reload(mod)
        new = getattr(mod, self.__class__.__name__)
        setattr(self, '__class__', new)
    
    def adjust_lengths(self, other):
        """
        Returns a pair of Clustering objects of equal lengths.
        If necessary extends the length of both filling the
        missing entities with `None`.
        Note: this works based on the entity labels. If no labels
        are available, raises `ValueError`.
        """
        if len(self) == len(other):
            return self, other
        elif self.labels is None or other.labels is None:
            raise ValueError('Missing labels, unable to identify entities.')
        else:
            def missing2none(c, labs):
                return \
                    Clustering(
                        list(
                            map(
                                lambda l:
                                    c.memberships[c.bylabel[l]] \
                                        if l in c.bylabel \
                                        else np.nan,
                                labs
                            )
                        ),
                        labels = list(map(
                                lambda l: l if l in c.bylabel else np.nan, labs
                            )),
                        name = c.name
                    )
            
            all_labels = sorted(set(self.labels) & set(other.labels))
            
            return (
                missing2none(self, all_labels),
                missing2none(other, all_labels)
            )
    
    def to_igraph(self):
        """
        Returns `igraph.Clustering` instance.
        """
        try:
            return igraph.Clustering(self.memberships)
        except:
            sys.stdout.write('No module `igraph` available.\n')
    
    def get_members(self, cluster):
        """
        Returns the indices of the members of one cluster.
        """
        if type(cluster) is not int or cluster >= self.cnum \
            and cluster in self.clab2cnum:
            cluster = self.clab2cnum[cluster]
        return self.members[cluster]
    
    def confusion_matrix(self, other):
        """
        Returns the confusion matrix between this clustering
        and one other.
        """
        cm = np.zeros((self.cnum, other.cnum))
        cm.dtype = np.int
        for a, ca in enumerate(self.clusters):
            ma = set(self.get_members(ca))
            for b, cb in enumerate(other.clusters):
                cm[a,b] = len(set(other.get_members(cb)) & ma)
        return cm
    
    def _pair_counts(self, other):
        """
        Returns the number of element pairs which are classified in the same
        cluster in both this clustering and the other, or classified in the
        same in this but different clusters in the other, or different in
        this, but the same in other, or different in both clusterings,
        respectively.
        Returns a tuple of four integers.
        """
        def pairs():
            return \
                map(
                    lambda i:
                        map(
                            lambda j:
                                (i, j),
                            xrange(i + 1, len(self))
                        ),
                    xrange(len(self) - 1)
                )
        
        def count(m1, m2, op1, op2):
            return sum(
                map(
                    lambda s:
                        len(
                            list(
                                filter(
                                    lambda ij:
                                        getattr(m1[ij[0]], op1)(m1[ij[1]]) and \
                                        getattr(m2[ij[0]], op2)(m2[ij[1]]),
                                    s
                                )
                            )
                        ),
                    pairs()
                )
            )
        
        return (
            count(self.memberships, other.memberships, '__eq__', '__eq__'),
            count(self.memberships, other.memberships, '__eq__', '__ne__'),
            count(self.memberships, other.memberships, '__ne__', '__eq__'),
            count(self.memberships, other.memberships, '__ne__', '__ne__')
        )
    
    @check_lengths
    def _pair_counts2(self, other):
        """
        Returns the number of element pairs which are classified in the same
        cluster in both this clustering and the other, or classified in the
        same in this but different clusters in the other, or different in
        this, but the same in other, or different in both clusterings,
        respectively.
        Returns a tuple of four integers.
        """
        
        p1 = comem.comembership(self)
        p2 = comem.comembership(other)
        n = len(p1)
        
        a11 = p1 * p2
        
        n11 = a11.sum()
        n10 = (p1 - a11).sum()
        n01 = (p2 - a11).sum()
        n00 = n - n10 - n01 - n11
        
        return n11, n10, n01, n00
    
    @check_lengths
    def _pair_counts3(self, other):
        """
        Returns the number of element pairs which are classified in the same
        cluster in both this clustering and the other, or classified in the
        same in this but different clusters in the other, or different in
        this, but the same in other, or different in both clusterings,
        respectively.
        Returns a tuple of four integers.
        """
        n = len(self)
        cmem1 = self.memberships.reshape([n,1]) == self.memberships.reshape([1,n])
        cmem2 = other.memberships.reshape([n,1]) == other.memberships.reshape([1,n])
        n11 = int(((cmem1 == cmem2).sum() - n) / 2)
        n10 = int((cmem1.sum() - n) / 2) - n11
        n01 = int((cmem2.sum() - n) / 2) - n11
        n00 = n - n11 - n10 - n01
        return n11, n10, n01, n00
    
    @check_lengths
    def pair_counts(self, other):
        """
        Returns the number of element pairs which are classified in the same
        cluster in both this clustering and the other, or classified in the
        same in this but different clusters in the other, or different in
        this, but the same in other, or different in both clusterings,
        respectively.
        Returns a tuple of four integers.
        """
        a = self.memberships
        b = other.memberships
        
        n = a.shape[0]  # also b.shape[0]
        
        counts_a = np.bincount(a)
        counts_b = np.bincount(b)
        sorter_a = np.argsort(a)
        
        n11 = 0
        same_a_offset = np.cumsum(counts_a)
        for indices in np.split(sorter_a, same_a_offset):
            b_check = b[indices]
            n11 += np.count_nonzero(b_check == b_check[:,None])
        
        n11 = (n11-n) // 2
        n10 = (np.sum(counts_a**2) - n) // 2 - n11
        n01 = (np.sum(counts_b**2) - n) // 2 - n11
        n00 = (n**2 - n) // 2 - n11 - n10 - n01
        
        return n11, n10, n01, n00
    
    @check_lengths
    def pair_counts5(self, other):
        return comem.pair_counts(self.memberships, other.memberships)
    
    def comembership(self):
        """
        Returns comembership vector, where each value tells if one pair
        of entites belong to the same group (1) or not (0).
        """
        co = []
        for i in xrange(0, len(self.memberships) - 1):
            for j in xrange(i + 1, len(self.memberships)):
                if self.memberships[i] == self.memberships[j]:
                    co.append(1)
                else:
                    co.append(0)
        return np.array(co)
    
    @check_lengths
    def _pair_counts_comem(self, other):
        """
        Returns pair counts calculated the naive way,
        from comembership vectors.
        This is slow, only here for testing.
        """
        
        co1 = self.comembership()
        co2 = other.comembership()
        
        return (
            sum(np.logical_and(co1 == 1, co2 == 1)),
            sum(np.logical_and(co1 == 1, co2 == 0)),
            sum(np.logical_and(co1 == 0, co2 == 1)),
            sum(np.logical_and(co1 == 0, co2 == 0))
        )
    
    @check_lengths
    def chi_squared(self, other, cm = None):
        """
        Returns the Chi-squared coefficient calculated between this
        clustering and one other.
        """
        cm = self.confusion_matrix(other) if cm is None else cm
        cs = 0.0
        n = float(len(self))
        for i in xrange(self.cnum):
            for j in xrange(other.cnum):
                e = self.sizes[i] * other.sizes[j] / n
                cs += (cm[i,j] - e) ** 2 / e
        return cs
    
    @check_lengths
    def rand(self, other):
        """
        Returns the Rand index calculated between this
        clustering and one other.
        """
        n11, n10, n01, n00 = self.pair_counts(other)
        n = float(len(self))
        return 2 * (n11 + n00) / n / (n - 1)
    
    @check_lengths
    def rand_adjusted(self, other, cm = None):
        """
        Returns the adjusted Rand index calculated between this
        clustering and one other.
        """
        cm = self.confusion_matrix(other) if cm is None else cm
        n = float(len(self))
        t1 = sum(map(lambda s: sp.misc.comb(s, 2), self.sizes.values()))
        t2 = sum(map(lambda s: sp.misc.comb(s, 2), other.sizes.values()))
        t3 = 2 * t1 * t2 / n / (n - 1)
        return \
            (sum(
                map(
                    lambda cmrow:
                        sum(
                            map(
                                lambda cmval:
                                    sp.misc.comb(cmval, 2),
                                cmrow
                            )
                        ),
                    cm
                )
            ) - t3) / ((t1 + t2) / 2.0 - t3)
    
    @check_lengths
    def _fowlkes_mallows_depr(self, other, cm = None):
        """
        Returns the Fowlkes-Mallows index calculated between this
        clustering and one other.
        
        This method gives different result than the other, despite
        these formulas are equal according to the paper. I suggest
        to use the other.
        """
        cm = self.confusion_matrix(other) if cm is None else cm
        n = float(len(self))
        return (cm ** 2).sum() - n / np.sqrt(
                (np.array(list(self.sizes.values())) ** 2 - n).sum() * \
                (np.array(list(other.sizes.values())) ** 2 - n).sum()
            )
    
    @check_lengths
    def fowlkes_mallows(self, other):
        """
        Returns the Fowlkes-Mallows index calculated between this
        clustering and one other.
        """
        n11, n10, n01, n00 = self.pair_counts(other)
        return n11 / np.sqrt((n11 + n10) * (n11 + n01))
    
    @check_lengths
    def mirkin(self, other, cm = None):
        """
        Returns the Mirkin metric (equivalence mismatch distance)
        calculated between this clustering and one other.
        """
        cm = self.confusion_matrix(other) if cm is None else cm
        return \
            (np.array(list(self.sizes.values())) ** 2).sum() + \
            (np.array(list(other.sizes.values())) ** 2).sum() - \
            2 * (cm ** 2).sum()
    
    @check_lengths
    def jaccard(self, other):
        """
        Returns the Jaccard index calculated between this
        clustering and one other.
        """
        n11, n10, n01, n00 = self.pair_counts(other)
        return n11 / float(n11 + n10 + n01)
    
    @check_lengths
    def partition_difference(self, other):
        """
        Returns the partiton difference calculated between this
        clustering and one other.
        """
        n11, n10, n01, n00 = self.pair_counts(other)
        return n00
    
    @check_lengths
    def f_measure(self, other, cm = None):
        """
        Returns the F-measure (F1 score) calculated between this
        clustering and one other.
        Please be aware that this metric is not symmetric.
        This clustering considered to be the natural classes,
        while the other used as the prediction.
        """
        cm = self.confusion_matrix(other) if cm is None else cm
        return \
            sum(
                map(
                    lambda i:
                        self.sizes[i] * \
                        np.nanmax(
                            list(
                                map(
                                    lambda j:
                                        2 * \
                                        cm[i,j] / float(self.sizes[i]) * \
                                        cm[i,j] / float(other.sizes[j]) / (
                                        cm[i,j] / float(self.sizes[i]) +
                                        cm[i,j] / float(other.sizes[j])
                                        ),
                                    filter(
                                        lambda j:
                                            self.sizes[i] > 0 or \
                                            other.sizes[j] > 0,
                                        xrange(other.cnum)
                                    )
                                )
                            )
                        ),
                    xrange(self.cnum)
                )
            ) / float(len(self))
    
    @check_lengths
    def meila_heckerman(self, other, cm = None):
        """
        Returns the Meila-Heckerman measure calculated between this
        clustering and one other.
        Please be aware that this metric is not symmetric.
        This clustering considered to be the optimal one,
        and the other is to be scored.
        """
        cm = self.confusion_matrix(other) if cm is None else cm
        return cm.max(axis = 0).sum() / float(len(self))
    
    @check_lengths
    def maximum_match(self, other, cm = None):
        """
        Returns the maximum match measure calculated between this
        clustering and one other.
        """
        def next_max(ma):
            mx = ma.max()
            am = ma.argmax()
            i = am // ma.shape[1]
            j = am % ma.shape[1]
            ma.mask[i,:] = True
            ma.mask[:,j] = True
            return mx
        
        mm = 0.0
        cm = self.confusion_matrix(other) if cm is None else cm
        cmm = np.ma.array(cm, mask = False)
        return sum(map(lambda _: next_max(cmm), xrange(min(cm.shape))))
    
    @check_lengths
    def van_dongen(self, other, cm = None):
        """
        Returns the Van Dongen measure calculated between this
        clustering and one other.
        """
        cm = self.confusion_matrix(other) if cm is None else cm
        return 2 * len(self) - cm.max(axis = 0).sum() - cm.max(axis = 1).sum()
    
    @check_lengths
    def mutual_information(self, other, cm = None):
        """
        Returns the mutual information calculated between this
        clustering and one other.
        """
        cm = self.confusion_matrix(other) if cm is None else cm
        n = float(len(self))
        return \
            sum(
                map(
                    lambda i:
                        sum(
                            map(
                                lambda j:
                                    0.0 if cm[i,j] == 0 else \
                                    cm[i,j] / n * np.log2(
                                        cm[i,j] * n / \
                                        float(self.sizes[i]) / \
                                        float(other.sizes[j])
                                    ),
                                xrange(other.cnum)
                            )
                        ),
                    xrange(self.cnum)
                )
            )
    
    def entropy(self):
        """
        Returns the Shannon entropy of the clustering.
        """
        n = float(len(self))
        return -1 * sum(
                map(
                    lambda s:
                        np.log2(s / n) * s / n,
                    self.sizes.values()
                )
            )
    
    @check_lengths
    def strehl_ghosh_mi(self, other, cm = None):
        """
        Returns the Strehl-Ghosh normalized mutual information
        calculated between this clustering and one other.
        """
        cm = self.confusion_matrix(other) if cm is None else cm
        mi = self.mutual_information(other, cm)
        return mi / np.sqrt(self.entropy() * other.entropy())
    
    @check_lengths
    def fred_jain_mi(self, other, cm = None):
        """
        Returns the Fred-Jain normalized mutual information
        calculated between this clustering and one other.
        This metric is identical to the Danon normalized MI
        from igraph.
        """
        cm = self.confusion_matrix(other) if cm is None else cm
        mi = self.mutual_information(other, cm)
        return 2 * mi / (self.entropy() + other.entropy())
    
    @check_lengths
    def variation_of_information(self, other, cm = None):
        """
        Returns the variation of information metric
        calculated between this clustering and one other.
        """
        cm = self.confusion_matrix(other) if cm is None else cm
        mi = self.mutual_information(other, cm)
        return self.entropy() + other.entropy() - 2 * mi
    
    @check_lengths
    def igraph_method(self, other, method, remove_none = True):
        """
        Wrapper for calling any method provided by
        `igraph.clustering.compare_communities()`.
        
        :param str method: The measure to calculate. Available metrics are
            "vi" or "meila" -- variation of information of Meila
            "nmi" or "danon" -- normalized mutual information of Danon
            "split-join" -- split-join distance of van Dongen
            "rand" -- Rand index of Rand
            "adjusted_rand" -- adjusted Rand index of Hubert and Arabie
        
        See more at
        http://igraph.org/python/doc/igraph.clustering-module.html#compare_communities
        """
        c1 = self.to_igraph()
        c2 = other.to_igraph()
        return igraph.clustering.compare_communities(c1, c2, method = method,
                                                     remove_none = remove_none)
    
    def variation_of_information_igraph(self, other, **kwargs):
        """
        Calculates the variation of information between this clustering and
        one other using igraph's implementation.
        """
        return self.igraph_method(other, method = 'vi', **kwargs)
    
    def danon_mi_igraph(self, other, **kwargs):
        """
        Calculates the Danon normalized mutual information
        between this clustering and one other using igraph's implementation.
        This is the same as Fred-Jain normalized MI.
        """
        return self.igraph_method(other, method = 'nmi', **kwargs)
    
    def split_join_distance_igraph(self, other, **kwargs):
        """
        Calculates the van Dongen split-join distance
        between this clustering and one other using igraph's implementation.
        """
        return self.igraph_method(other, method = 'split-join', **kwargs)
    
    def rand_igraph(self, other, **kwargs):
        """
        Calculates the Rand index between this clustering and one other
        using igraph's implementation.
        """
        return self.igraph_method(other, method = 'rand', **kwargs)
    
    def rand_adjusted_igraph(self, other, **kwargs):
        """
        Calculates Hubert and Arabie's adjusted Rand index
        between this clustering and one other using igraph's implementation.
        """
        return self.igraph_method(other, method = 'adjusted_rand', **kwargs)
    
    def pearson(self, other, **kwargs):
        """
        Pearson correlation between pair vectors.
        """
        #n11, n10, n01, n00 = self.pair_counts4(other)
        n11, n10, n01, n00 = self.pair_counts5(other)
        
        n = n11 + n10 + n01 + n00
        
        # to avoid division by zero in any cases
        if n == 0 or \
            (n10 == 0 and n01 == 0 and n00 == 0) or \
            (n11 == 0 and (n10 == 0 or n01 == 0)):
            return 0.0
        
        # means
        a1 = (n10 + n11) / np.float128(n)
        a2 = (n01 + n11) / np.float128(n)
        
        # Pearson's r
        num = n11 - n * a1 * a2
        den = np.sqrt((n11 + n10) - n * a1**2) * np.sqrt((n11 + n01) - n * a2**2)
        
        return num / den
