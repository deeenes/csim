#!/usr/bin/python3
# -*- coding: utf-8 -*-

#
#  This file is part of the `csim` Python module
#
#  Copyright (c) 2016 - EMBL-EBI
#
#  File author(s): Dénes Türei (turei.denes@gmail.com)
#
#  Distributed under the GPLv3 License.
#  See accompanying file LICENSE.txt or copy at
#      http://www.gnu.org/licenses/gpl-3.0.html
#
#  Website: http://www.ebi.ac.uk/~denes
#

from csim.main import *
