#!/usr/bin/python
# -*- coding: utf-8 -*-

# (c) Dénes Türei, EMBL-EBI 2016

import csim
from csim import comem
import os
import seaborn as sns
import cProfile

datadir = os.path.join('..', 'Baseline_methods')

cs = csim.ClusteringSet()
cs.add_file_list(list(map(lambda f: os.path.join(datadir, f), os.listdir(datadir))),
                 param = {'cols': 2, 'members_list': True, 'clnames': 0})

a1 =  cs.clusterings[-1].memberships
a2 =  cs.clusterings[-2].memberships

pr = cProfile.Profile()
pr.enable()
comem.comem(a1, a2)
pr.disable()
pr.print_stats()

pr = cProfile.Profile()
pr.enable()
cs.pearson()
pr.disable()
pr.print_stats()

cs.danon_mi_igraph()

def name_format(name):
    n = name.split('_')
    return '%s_%s_%s' % (n[0][0], n[2][0], ''.join(n[-1].split('.')[:-1]))

cs.heatmap('danon_mi_igraph',
           fname = 'nmi_sc1_communities.pdf',
           title = 'Normalized mutual information (Fred & Jain) over all clusterings',
           name_format = name_format,
           context = {'figure.figsize': [12, 10.5], 'ytick.labelsize': 4, 'xtick.labelsize': 4})

cs.heatmap('pearson',
           fname = 'pearsonr_baseline_communities.pdf',
           title = 'Pearson correlation of comemberships over all clusterings',
           name_format = name_format,
           context = {'figure.figsize': [12, 10.5], 'ytick.labelsize': 4, 'xtick.labelsize': 4})

cs.methods_scatterplot('pearson', 'danon_mi_igraph',
                       fname = 'pearsonr_nmi_scatter.pdf',
                       title = 'NMI vs. Pearson\'s r over all pairs of clusterings',
                       xlab = 'Pearson\'s r',
                       ylab = 'Normalized mutual information',
                       context = 'paper',
                       font_scale = 2.4,
                       rc = {'figure.figsize': [10.5, 10.5],
                             #'xtick.labelsize': 24, 'ytick.labelsize': 24
                             },
                       line_kws = {'color': '#e74c3c', 'alpha': 0.5},
                       scatter_kws = {'color': '#3498db', 's': 17, 'alpha': 0.2}
                       )
