#!/usr/bin/python
# -*- coding: utf-8 -*-

# (c) Dénes Türei, EMBL-EBI 2016

import csim
from csim import comem
import os
import seaborn as sns
import cProfile
import itertools

datadir = os.path.join('..', 'data', 'Submissions', 'subchallenge_1')
fn_submids = os.path.join('..', 'data', 'Submissions', 'submissions_subchallenge1.txt')

all_files = \
    list(
        itertools.chain(
            *map(
                lambda subdir:
                    map(
                        lambda f:
                            os.path.join(datadir, subdir, f),
                        os.listdir(os.path.join(datadir, subdir))
                    ),
                os.listdir(datadir)
            )
        )
    )

cs = csim.ClusteringSet()
cs.add_file_list(all_files,
                 param = {'cols': 2, 'members_list': True, 'clnames': 0})

cs.danon_mi_igraph()

def name_format(name):
    # 3344018.7329532.6_homology_anonym_v2.txt.gz
    n = name.split('_')
    return 'sample' if name == 'sample.txt' \
        else '%s_%s' % (n[1][:2].upper(), sumids['.'.join(n[0].split('.')[:2])])


cs.heatmap('danon_mi_igraph',
           fname = 'nmi_sc1_communities.pdf',
           title = 'Normalized mutual information (Fred & Jain) over all clusterings',
           name_format = name_format,
           context = {'figure.figsize': [12, 10.5], 'ytick.labelsize': 2, 'xtick.labelsize': 2})

cs.save_matrix('danon_mi_igraph', 'sc1_nmi.tab', name_format = name_format)

# read team/submission names:
with open(fn_submids, 'r') as f:
    sumids = \
        dict(
            map(
                lambda l:
                    ('%s.%s' % (l[0], l[1]), l[4][:9]),
                list(
                    map(
                        lambda l:
                            l.split('\t'),
                        f
                    )
                )[1:]
            )
        )
