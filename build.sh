#!/bin/bash

rm -f src/*.pyc
rm -f src/csim/*.pyc
rm -rf src/csim.egg-info

sed -i 's/\([0-9]*\.[0-9]*\.\)\([0-9]*\)/echo \1$\(\(\2+1\)\)/ge' src/csim/__version__

python setup.py sdist
