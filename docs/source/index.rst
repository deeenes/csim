.. csim documentation master file, created by
   sphinx-quickstart on Fri Sep  9 16:23:00 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to csim's documentation!
================================

.. toctree::
   :maxdepth: 2

Source code
+++++++++++

csim is free software, licensed under GPLv3_.
The code is available at bitbucket_.

.. _GPLv3: http://www.gnu.org/licenses/gpl.html
.. _bitbucket: https://bitbucket.org/deeenes/csim

Features
++++++++

- Comparison (similarity/distance) between flat disjunct clusterings or network community structures
- 20 similarity metrics available
- 5 of these implemented in igraph_
- `Reader` class for reading clustering data from files
- `ClusteringSet` class for analysis a set of different clusterings on the same set of entities

.. _igraph: http://igraph.org/

Quick start
+++++++++++

.. code:: python

        import csim

        # instantiate the main class:
        cs = csim.ClusteringSet()

        # read clusterings data from multiple files:
        cs.add_file_list(['clustering1.csv', 'clustering2.csv'], 
                         param = {'header': True, 'sep': ';', 'cols': [1]})

        # obtain a similarity matrix using the igraph implementation of
        # Danon adjusted mutual information:
        sim = cs.danon_mi_igraph()

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Reference
=========

.. automodule:: csim
   :members:

.. automodule:: csim.main
   :members:

